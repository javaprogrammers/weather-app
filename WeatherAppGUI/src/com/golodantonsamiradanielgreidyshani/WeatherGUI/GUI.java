package com.golodantonsamiradanielgreidyshani.WeatherGUI;

import com.golodantonsamiradanielgreidyshani.OpenWeatherMap.IWeatherDataService;
import com.golodantonsamiradanielgreidyshani.OpenWeatherMap.Location;
import com.golodantonsamiradanielgreidyshani.OpenWeatherMap.WeatherDataServiceException;
import com.golodantonsamiradanielgreidyshani.OpenWeatherMap.WeatherDataServiceFactory;

import com.golodantonsamiradanielgreidyshani.XmlInformation.WeatherData;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.dial.*;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.ui.GradientPaintTransformType;
import org.jfree.ui.StandardGradientPaintTransformer;
import org.xml.sax.SAXException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;

/**
 * Graphic User interface
 * Show's to user an interface of weather app
 */
public class GUI
{
    IWeatherDataService service =
            WeatherDataServiceFactory.getWeatherDataService(WeatherDataServiceFactory.OPEN_WEATHER_MAP);

    private WeatherData weatherData;

    private JButton btnSubmit;
    private JButton btnExit;
    private Button btnCelsius;
    private Button btnFahrenheit;
    private Button btnKelvin;
    private JFrame frame;
    private JLabel lblCity;
    private JLabel lblCountry;
    private JLabel lblNewLabelTemperature;
    private JLabel lblGetAt;
    private JTextField textFieldCity;
    private JTextField textFieldCountry;
    private JTable table;
    private DefaultTableModel defaultTableModelTable;
    private JLabel lblNewLabelCityCountry;
    private JLabel lblDate;
    private JLabel lblNewLabelDegreeIcon;
    private JLabel lblBackgroundImage;
    private JLabel lblWeatherIcon;
    private Image imgBackgroundImage;
    private Image imgWeatherIcon;
    private URL urlBackGrounImage;
    private URL urlWeahterIcon;
    private DefaultValueDataset temperatureValue;
    private StandardDialScale standarddialscale;
    private DialTextAnnotation dialtextannotation;

    /**
     * Launch the application.
     */
    public static void main(String[] args)
    {
        EventQueue.invokeLater(() -> {
            GUI window = new GUI();
            window.frame.setVisible(true);
        });
    }

    /**
     * Create the application.
     */
    public GUI()
    {
        try
        {
            initialize();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (WeatherDataServiceException e)
        {
            JOptionPane.showMessageDialog(frame, e.getMessage());
        }
    }

    /**
     * Initialize the contents of the frame.
     */
    private <AutoCompleteSupport, AutocompleteJComboBox> void initialize()
            throws IOException, WeatherDataServiceException
    {
        //Frame
        frame = new JFrame("Wethaer App by Nathan, Dani & Shani");
        frame.setResizable(false);
        frame.setBounds(100, 100, 815, 420);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        //DemoPanel
        new Termostat();

        //JButtons
        btnSubmit = new JButton("Submit");
        btnSubmit.setBounds(440, 11, 80, 20);

        btnExit = new JButton("Exit");
        btnExit.setBounds(690, 347, 105, 23);

        //Buttons
        btnCelsius = new Button("\u00B0C");
        setBtnAttribute(btnCelsius, "David", Font.BOLD, 13, new Color(247, 215, 117), 315, 11, 30, 20);

        btnFahrenheit = new Button("\u00B0F");
        setBtnAttribute(btnFahrenheit, "David", Font.BOLD, 12, new Color(64, 181, 244), 351, 11, 30, 20);

        btnKelvin = new Button("\u00B0K");
        setBtnAttribute(btnKelvin, "David", Font.BOLD, 12, new Color(64, 181, 244), 389, 11, 30, 20);

        //JTextFields
        textFieldCountry = new JTextField();
        textFieldCountry.setBounds(199, 11, 98, 20);

        textFieldCity = new JTextField();
        textFieldCity.setBounds(41, 11, 98, 20);

        //Table
        table = new JTable()
        {
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
            {
                Component component = super.prepareRenderer(renderer, row, column);

                component.setBackground(Color.white);

                if(!isRowSelected(row))
                {
                    component.setBackground(row % 2 == 1 ? getBackground() : new Color(198, 185, 197));
                }

                return component;
            }
        };

        initTable();

        //JLabels
        lblCity = new JLabel("City:");
        lblCity.setBounds(10, 14, 46, 14);

        lblCountry = new JLabel("Country:");
        lblCountry.setBounds(149, 14, 70, 14);

        lblNewLabelCityCountry = new JLabel("City, Country");
        setLabelAttributes(lblNewLabelCityCountry, "David", Font.BOLD, 25, 41, 60, 230, 44);
        lblNewLabelCityCountry.setVisible(false);

        lblDate = new JLabel("Date");
        setLabelAttributes(lblDate, "David", Font.BOLD, 20, 185, 122, 300, 23);
        lblDate.setVisible(false);

        lblNewLabelDegreeIcon = new JLabel("\u00B0C");
        setLabelAttributes(lblNewLabelDegreeIcon, "David", Font.BOLD, 25, 460, 73, 30, 23);
        lblNewLabelDegreeIcon.setVisible(false);

        lblNewLabelTemperature = new JLabel("Temperature");
        setLabelAttributes(lblNewLabelTemperature, "David", Font.BOLD, 28, 380, 71, 80, 25);
        lblNewLabelTemperature.setVisible(false);

        lblGetAt = new JLabel("Get at:");
        setLabelAttributes(lblGetAt, "David", Font.BOLD, 21, 120, 122, 65, 23);
        setLabelVisibleMode(lblGetAt, false);

        //Add all fields to Frame
        frame.getContentPane().add(btnSubmit);
        frame.getContentPane().add(btnExit);
        frame.getContentPane().add(btnCelsius);
        frame.getContentPane().add(btnFahrenheit);
        frame.getContentPane().add(btnKelvin);
        frame.getContentPane().add(lblNewLabelCityCountry);
        frame.getContentPane().add(lblCity);
        frame.getContentPane().add(lblCountry);
        frame.getContentPane().add(textFieldCity);
        frame.getContentPane().add(table);
        frame.getContentPane().add(lblNewLabelCityCountry);
        frame.getContentPane().add(lblDate);
        frame.getContentPane().add(lblNewLabelDegreeIcon);
        frame.getContentPane().add(textFieldCountry);
        frame.getContentPane().add(lblNewLabelTemperature);
        frame.getContentPane().add(lblGetAt);

        startGettingWeatherDataFromSite();
        setBackgroungImage();
    }

    private void startGettingWeatherDataFromSite()
    {
        btnSubmit.addActionListener(e -> {
            if(textFieldCity.getText().length() != 0)
            {
                new Thread(() -> {
                    try
                    {
                        weatherData = service.getWeatherData(new Location(textFieldCity.getText(), textFieldCountry.getText()));

                        setWeatherIcon();
                        setBackgroungImage();
                        setLabelCityCountry();
                        setLabelTemperatuere();
                        setLabelDegree();
                        setLabelDate();
                        setLabelGetAt();
                        setTable();
                        setTemperatureIconMax();
                        setCurrentTemperatureInIcon();
                    }
                    catch (WeatherDataServiceException | SAXException | IOException e1)
                    {
                        showMessageToUser(e1.getMessage());
                    }
                }).start();
            }
            else
            {
                JOptionPane.showMessageDialog(frame, "Please enter a city name");
            }
        });

        btnExit.addActionListener(e -> System.exit(0));

        btnCelsius.addActionListener(e -> {
            Location.setTemperatureUnit("metric");

            setBtnAttribute(btnCelsius, "David", Font.BOLD, 13, new Color(247, 215, 117));
            setBtnAttribute(btnFahrenheit, "David", Font.BOLD, 12, new Color(64, 181, 244));
            setBtnAttribute(btnKelvin, "David", Font.BOLD, 12, new Color(64, 181, 244));

            btnSubmit.doClick();
        });

        btnFahrenheit.addActionListener(e -> {
            Location.setTemperatureUnit("Imperial");

            setBtnAttribute(btnFahrenheit, "David", Font.BOLD, 13, new Color(247, 215, 117));
            setBtnAttribute(btnCelsius, "David", Font.BOLD, 12, new Color(64, 181, 244));
            setBtnAttribute(btnKelvin, "David", Font.BOLD, 12, new Color(64, 181, 244));
            btnSubmit.doClick();
        });

        btnKelvin.addActionListener(e -> {
            Location.setTemperatureUnit("kelvin");

            setBtnAttribute(btnKelvin, "David", Font.BOLD, 13, new Color(247, 215, 117));
            setBtnAttribute(btnCelsius, "David", Font.BOLD, 12, new Color(64, 181, 244));
            setBtnAttribute(btnFahrenheit, "David", Font.BOLD, 12, new Color(64, 181, 244));
            btnSubmit.doClick();
        });

        textFieldCity.addActionListener(e1 -> btnSubmit.doClick());

        textFieldCountry.addActionListener(e1 -> btnSubmit.doClick());
    }

    private void showMessageToUser(String msgForUser)
    {
        JOptionPane.showMessageDialog(frame, msgForUser);
    }

    private void setTemperatureIconMax()
    {
        if(Objects.equals(weatherData.getTemperature().getUnit().toString().toLowerCase(), "metric"))
        {
            standarddialscale.setUpperBound(60);
            standarddialscale.setLowerBound(-60);
            dialtextannotation.setLabel("Temperature " + "\u00B0C");
        }
        else if(Objects.equals(weatherData.getTemperature().getUnit().toString().toLowerCase(), "imperial"))
        {
            standarddialscale.setUpperBound(140);
            standarddialscale.setLowerBound(-60);
            dialtextannotation.setLabel("Temperature " + "\u00B0F");
        }
        else if(Objects.equals(weatherData.getTemperature().getUnit().toString().toLowerCase(), "kelvin"))
        {
            standarddialscale.setUpperBound(330);
            standarddialscale.setLowerBound(213);
            dialtextannotation.setLabel("Temperature " + "\u00B0K");
        }
    }

    private void setCurrentTemperatureInIcon()
    {
        temperatureValue.setValue(Double.parseDouble(String.valueOf(weatherData.getTemperature().getValue())));
    }

    private void setWeatherIcon() throws WeatherDataServiceException, IOException
    {
        try
        {
            if(lblWeatherIcon != null)
            {
                frame.getContentPane().remove(lblWeatherIcon);
            }

            urlWeahterIcon = new URL("http://openweathermap.org/img/w/" + weatherData.getWeather().getIcon() + ".png");

            imgWeatherIcon = ImageIO.read(urlWeahterIcon);

            lblWeatherIcon = new JLabel(new ImageIcon(imgWeatherIcon));
            frame.getContentPane().setLayout(null);

            frame.getContentPane().add(lblWeatherIcon);
            lblWeatherIcon.setBounds(0, 0, 650, 170);

            frame.setVisible(true);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void setBackgroungImage() throws WeatherDataServiceException, IOException
    {
        if(lblBackgroundImage != null)
        {
            frame.getContentPane().remove(lblBackgroundImage);
        }

        File file = new File("src/background.jpeg");

        imgBackgroundImage = ImageIO.read(file);

        lblBackgroundImage = new JLabel(new ImageIcon(imgBackgroundImage));

        frame.getContentPane().setLayout(null);
        frame.getContentPane().add(lblBackgroundImage);

        lblBackgroundImage.setBounds(0, 0, 900, 385);

        frame.setVisible(true);
    }

    private void initTable()
    {
        defaultTableModelTable =
                new DefaultTableModel(new Object[][]{{" Wind", null}, {" Cloudiness", null}, {" Pressure", null},
                        {" Humidity", null}, {" Sunrise", null}, {" Sunset\t", null},
                        {" Geo coords\t", null},}, new String[]{"New column", "New column"});

        table.setEnabled(false);
        table.setFont(new Font("Tahoma", Font.PLAIN, 15));
        table.setModel(defaultTableModelTable);
        table.setBorder(new LineBorder(new Color(0, 0, 0)));


        table.getColumnModel().getColumn(0).setPreferredWidth(0);
        table.setRowHeight(30);
        table.getColumnModel().getColumn(1).setPreferredWidth(230);
        table.setBounds(10, 160, 510, 210);
    }

    private void setTable()
    {
        defaultTableModelTable.setValueAt(getWindAtrribute(), 0, 1);
        defaultTableModelTable.setValueAt(getCloudnesssAttribute(), 1, 1);
        defaultTableModelTable.setValueAt(getPressureAttribute(), 2, 1);
        defaultTableModelTable.setValueAt(getHumidityAtrribute(), 3, 1);
        defaultTableModelTable.setValueAt(getSunRiseAttribute(), 4, 1);
        defaultTableModelTable.setValueAt(getSunSetAttribute(), 5, 1);
        defaultTableModelTable.setValueAt(getCoordsAtrribute(), 6, 1);

        table.setModel(defaultTableModelTable);
    }

    public String getWindAtrribute()
    {
        return weatherData.getWind().toString();
    }

    private String getCloudnesssAttribute()
    {
        return weatherData.getClouds().toString();
    }

    private String getPressureAttribute()
    {
        return weatherData.getPressure().toString();
    }

    private String getHumidityAtrribute()
    {
        return weatherData.getHumidity().toString();
    }

    private String getSunRiseAttribute()
    {
        return weatherData.getCity().getSun().msgSunRise().toString();
    }

    private String getSunSetAttribute()
    {
        return weatherData.getCity().getSun().msgSunSet();
    }

    private String getCoordsAtrribute()
    {
        return weatherData.getCity().getCoord().toString();
    }

    private void setBtnAttribute(Button currentBtn, String fontType, int boldAmount, int fontSize, Color color,
                                 int coordinateX, int coordinateY, int width, int height)
    {
        setBtnAttribute(currentBtn, fontType, boldAmount, fontSize, color);
        currentBtn.setBounds(coordinateX, coordinateY, width, height);
    }

    private void setBtnAttribute(Button currentBtn, String fontType, int boldAmount, int fontSize, Color color)
    {
        currentBtn.setFont(new Font(fontType, boldAmount, fontSize));
        currentBtn.setBackground(color);
    }

    private void setLabelAttributes(JLabel currentLable, String fontType, int boldAmount, int fontSize, int coordinateX,
                                    int coordinateY, int width, int height)
    {
        setLabelAttributes(currentLable, fontType, boldAmount, fontSize);
        currentLable.setBounds(coordinateX, coordinateY, width, height);
    }

    private void setLabelAttributes(JLabel currentBtn, String fontType, int boldAmount, int fontSize)
    {
        currentBtn.setFont(new Font(fontType, boldAmount, fontSize));
    }

    private void setLabelGetAt()
    {
        setLabelVisibleMode(lblGetAt, true);
    }

    private void setLabelDate()
    {
        setLabelVisibleMode(lblDate, true);
        lblDate.setText(weatherData.getLastUpdate().getValue().toString());
    }

    private void setLabelDegree()
    {
        setLabelVisibleMode(lblNewLabelTemperature, true);
        lblNewLabelTemperature.setText(String.valueOf(weatherData.getTemperature().getValue()));
        lblNewLabelTemperature.setSize(lblNewLabelTemperature.getPreferredSize());
    }

    private void setLabelTemperatuere()
    {
        setLabelVisibleMode(lblNewLabelDegreeIcon, true);
        lblNewLabelDegreeIcon.setText(getCurrentTemperatureUnit());
    }

    private void setLabelCityCountry()
    {
        setLabelVisibleMode(lblNewLabelCityCountry, true);
        lblNewLabelCityCountry.setText(weatherData.getCity().getName() + ", " + weatherData.getCity().getCountryCode());
    }
    private void setLabelVisibleMode(JLabel currentLabel, boolean IsVisiable)
    {
        currentLabel.setVisible(IsVisiable);
    }

    private String getCurrentTemperatureUnit()
    {
        String currenttemperatureUnit = null;

        if(Objects.equals(weatherData.getTemperature().getUnit().toString().toLowerCase(), "metric"))
        {
            currenttemperatureUnit = "\u00B0C";
        }
        else if(Objects.equals(weatherData.getTemperature().getUnit().toString().toLowerCase(), "imperial"))
        {
            currenttemperatureUnit = "\u00B0F";
        }
        else if(Objects.equals(weatherData.getTemperature().getUnit().toString().toLowerCase(), "kelvin"))
        {
            currenttemperatureUnit = "\u00B0K";
        }

        return currenttemperatureUnit;
    }

    class Termostat extends JFrame
    {
        public Termostat()//CHANGE THE NAME
        {
            temperatureValue = new DefaultValueDataset(0);

            DialPlot dialplot = new DialPlot();

            dialplot.setView(0.0D, 0.0D, 1.0D, 1.0D);
            dialplot.setDataset(0, temperatureValue);

            StandardDialFrame standarddialframe = new StandardDialFrame();
            dialplot.setDialFrame(standarddialframe);

            GradientPaint gradientpaint =
                    new GradientPaint(new Point(), new Color(255, 255, 255), new Point(), new Color(170, 170, 220));
            DialBackground dialbackground = new DialBackground(gradientpaint);

            dialbackground.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.VERTICAL));
            dialplot.setBackground(dialbackground);

            dialtextannotation = new DialTextAnnotation("Temperature " + "\u00B0C");
            dialtextannotation.setFont(new Font("Dialog", 1, 14));
            dialtextannotation.setRadius(0.69999999999999996D);
            dialplot.addLayer(dialtextannotation);

            standarddialscale = new StandardDialScale(-40D, 60D, -120D, -300D, 10D, 4);
            standarddialscale.setTickRadius(0.88D);
            standarddialscale.setTickLabelOffset(0.14999999999999999D);
            standarddialscale.setTickLabelFont(new Font("Dialog", 0, 14));
            dialplot.addScale(0, standarddialscale);

            org.jfree.chart.plot.dial.DialPointer.Pin pin = new org.jfree.chart.plot.dial.DialPointer.Pin(1);
            pin.setRadius(0.55000000000000004D);
            dialplot.addPointer(pin);

            //Arrow
            org.jfree.chart.plot.dial.DialPointer.Pointer pointer =
                    new org.jfree.chart.plot.dial.DialPointer.Pointer(0);
            dialplot.addPointer(pointer);
            DialCap dialcap = new DialCap();
            dialcap.setRadius(0.10000000000000001D);
            dialplot.setCap(dialcap);
            dialplot.setBackground(null);

            //ChartFactory.create
            JFreeChart jfreechart = new JFreeChart(dialplot);
            jfreechart.setBackgroundPaint(null);
            ChartPanel chartpanel = new ChartPanel(jfreechart);
            chartpanel.setPreferredSize(new Dimension(150, 1));
            chartpanel.setOpaque(false);

            chartpanel.setBounds(550, 60, 230, 230);
            frame.getContentPane().setLayout(null);
            frame.getContentPane().add(chartpanel);
        }
    }
}
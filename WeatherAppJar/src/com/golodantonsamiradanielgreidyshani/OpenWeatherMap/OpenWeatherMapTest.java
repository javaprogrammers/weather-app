package com.golodantonsamiradanielgreidyshani.OpenWeatherMap;

import com.golodantonsamiradanielgreidyshani.XmlInformation.WeatherData;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class OpenWeatherMapTest
{
    private OpenWeatherMap openWeatherMap;

    @Before
    public void setUp() throws Exception
    {
        openWeatherMap = OpenWeatherMap.getInstance();
    }

    @After
    public void tearDown() throws Exception
    {
        openWeatherMap = null;
    }

    @Test
    public void testGetWeatherData() throws Exception
    {
        WeatherData expected =  openWeatherMap.getWeatherData(new Location("holon", "IL"));

        assertNotNull(expected);
    }
}
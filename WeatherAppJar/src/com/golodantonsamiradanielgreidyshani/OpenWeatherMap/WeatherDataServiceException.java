package com.golodantonsamiradanielgreidyshani.OpenWeatherMap;

/**
 * gets message exception and initializes extended class
 */
public class WeatherDataServiceException extends Exception
{
    public WeatherDataServiceException(String message, Throwable e)
    {
        super(message, e);
    }

    public WeatherDataServiceException(String message)
    {
        super(message);
    }
}

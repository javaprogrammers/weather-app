package com.golodantonsamiradanielgreidyshani.OpenWeatherMap;

import com.golodantonsamiradanielgreidyshani.XmlInformation.WeatherData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WeatherDataServiceFactoryTest
{
    private static IWeatherDataService weatherDataServiceFactory = null;
    public static final String OPEN_WEATHER_MAP = "OPEN_WEATHER_MAP";
    private IWeatherDataService service = null;

    @Before
    public void setUp() throws Exception
    {
        service =  WeatherDataServiceFactory.getWeatherDataService(WeatherDataServiceFactory.OPEN_WEATHER_MAP);
    }

    @After
    public void tearDown() throws Exception
    {
        weatherDataServiceFactory = null;
        service = null;
    }

    @Test
    public void testGetOpenWeatherMap() throws Exception
    {
        String expected = "OPEN_WEATHER_MAP";
        String actual = OPEN_WEATHER_MAP;

        assertEquals(expected, actual);
    }

    @Test
    public void testGetService() throws Exception
    {
        IWeatherDataService expected = WeatherDataServiceFactory.getWeatherDataService(WeatherDataServiceFactory.OPEN_WEATHER_MAP);
        assertEquals(expected, service);
    }

    @Test
    public void testGetWeatherDataService() throws Exception
    {
        IWeatherDataService expected = location ->
        {
            weatherDataServiceFactory = OpenWeatherMap.getInstance();

            return (WeatherData)weatherDataServiceFactory;
        };

        assertNotNull(expected);
    }
}
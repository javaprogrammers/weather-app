package com.golodantonsamiradanielgreidyshani.OpenWeatherMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LocationTest
{
    private Location location;

    @Before
    public void setUp() throws Exception
    {
        location = new Location("holon", "IL");
        Location.setTemperatureUnit("imperial");
    }

    @After
    public void tearDown() throws Exception
    {
        location = null;
    }

    @Test
    public void testGetCity() throws Exception
    {
        String expected = "holon";
        String actual = location.getCity();

        assertEquals(expected, actual);
    }

    @Test
    public void testGetCountry() throws Exception
    {
        String expected = "IL";
        String actual = location.getCountry();

        assertEquals(expected, actual);
    }

    @Test
    public void testGetTemperatureUnit() throws Exception
    {
        String expected = "imperial";
        String actual = Location.getTemperatureUnit();

        assertEquals(expected, actual);
    }
}
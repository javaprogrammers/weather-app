package com.golodantonsamiradanielgreidyshani.OpenWeatherMap;

import com.golodantonsamiradanielgreidyshani.XmlInformation.WeatherData;
import org.xml.sax.SAXException;

import java.io.IOException;

/**
 * interface holds methods which get information from web services
 */
public interface IWeatherDataService
    {
        WeatherData getWeatherData(Location location)
                throws WeatherDataServiceException, SAXException, IOException;
    }

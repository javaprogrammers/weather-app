package com.golodantonsamiradanielgreidyshani.OpenWeatherMap;

/**
 * class WeatherDataServiceFactory factory for Weather Web services.
 * WeatherDataServiceFactory Class creates an object suitable for a given Web service.
 */
public class WeatherDataServiceFactory
{
    /**
     * OPEN_WEATHER_MAP (String) - A constant identifier for openweathermap
     *
     * weatherDataServiceFactory - (type IWeatherDataService)
     * represent a service that implement all the IWeatherDataService methods.
     */
    private static IWeatherDataService weatherDataServiceFactory = null;
    public static final String OPEN_WEATHER_MAP = "OPEN_WEATHER_MAP";

    /**
     * Get the identifier for Open Weather Map Service.
     *
     * @return String - The key for openweathermap service
     */
    public static String getOpenWeatherMap()
    {
        return OPEN_WEATHER_MAP;
    }

    /**
     * if the object was not created he will create it, but only once.
     *
     * @param value - get a specific web service by string value.
     * @return one and only instance of the weatherDataService requested by the String value.
     */
    public static IWeatherDataService getWeatherDataService(String value)
    {
        if(WeatherDataServiceFactory.OPEN_WEATHER_MAP.equals(value))
        {
            weatherDataServiceFactory = OpenWeatherMap.getInstance();
        }

        return weatherDataServiceFactory;
    }
}
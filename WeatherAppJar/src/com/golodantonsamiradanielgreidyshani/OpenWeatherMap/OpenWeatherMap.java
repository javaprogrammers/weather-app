package com.golodantonsamiradanielgreidyshani.OpenWeatherMap;

import com.golodantonsamiradanielgreidyshani.XmlInformation.WeatherData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 * this class defines a site url and stores the parsed values in a map (Collection)
 */
public class OpenWeatherMap implements IWeatherDataService
{
    private Map<String, String> map = new Hashtable<>();
    WeatherData weatherData = null;
    private final String k_ApiKey = "abfb39ab456bd0db1f144b289eaca3a3";

    private static OpenWeatherMap instance = null;

    /**
     * private constructor suppresses generation of a (public) default constructor
     * this constructor ensures singleton
     */
    private OpenWeatherMap()
    {
    }

    /**
     * @return reference to OpenWeatherMap object
     */
    public static OpenWeatherMap getInstance()
    {
        if (instance == null)
        {
            instance = new OpenWeatherMap();
        }

        return instance;
    }

    /**
     * receives class Location (Contains: cityName, countryCode)
     * defines a url by class Location with variables in it
     * the url is always the same except for city name, country name and temperature unit (it changes according to the users input)
     * if could not find a correct url, a message is thrown to the user
     * converts xml file to a document file
     * if a conversion fails, a message is thrown to the user
     * parsing a document according to attributes
     *
     * @param location - class that holds cityName, countryCode and temperatureUnit
     * @return reference to WeatherData object
     * @throws WeatherDataServiceException
     * @throws SAXException
     * @throws IOException
     */
    @Override
    public WeatherData getWeatherData(Location location) throws WeatherDataServiceException, SAXException, IOException
    {
        HttpURLConnection httpURLConnection = null;

        try
        {
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=" + location.getCity() + ","
                              + location.getCountry()
                              + "&units=" + Location.getTemperatureUnit()
                              + "&APPID=" + k_ApiKey + "&mode=xml");

            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            try (java.io.InputStream inputStream = httpURLConnection.getInputStream())
            {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document document = documentBuilder.parse(inputStream);

                NodeList rootNodeList = document.getElementsByTagName("current");
                Node rootNode = rootNodeList.item(0);
                setAllAttributes(rootNode);

                weatherData = sendToWeatherData();
            }
        }
        catch (ParserConfigurationException e1)
        {
            throw new WeatherDataServiceException("Can't parse the xml file.");
        }
        catch (MalformedURLException | UnknownHostException e)
        {
            throw new WeatherDataServiceException("No internet connection.");
        }
        catch (SAXException e)
        {
            throw new WeatherDataServiceException("No such city.");
        }
        finally
        {
            if(httpURLConnection != null)
            {
                httpURLConnection.disconnect();
            }
        }

        return weatherData;
    }

    /**
     * sets all attributes by current node name
     * @param rootNode - node that contains weather attributes
     */
    private void setAllAttributes(Node rootNode)
    {
        Element rootElement = (Element) rootNode;

        setAttribute("city", rootElement);
        setAttribute("coord", rootElement);
        setAttribute("country", rootElement);
        setAttribute("sun", rootElement);
        setAttribute("temperature", rootElement);
        setAttribute("humidity", rootElement);
        setAttribute("pressure", rootElement);
        setAttribute("speed", rootElement);
        setAttribute("gusts", rootElement);
        setAttribute("direction", rootElement);
        setAttribute("clouds", rootElement);
        setAttribute("visibility", rootElement);
        setAttribute("precipitation", rootElement);
        setAttribute("weather", rootElement);
        setAttribute("lastupdate", rootElement);
    }

    /**
     * for each element chosen by tag name from switch case, attributes are added to an ArrayList
     *
     * @param currentAttribute - the chosen attribute (of type string), inserted to the switch case
     * @param rootElement - contains attributes of the current element
     */
    private void setAttribute(String currentAttribute, Element rootElement)
    {
        ArrayList<String> arrayListOfSubAttributes = new ArrayList<>();
        NodeList elementsByTagName = rootElement.getElementsByTagName(currentAttribute);

        for (int i = 0; i < elementsByTagName.getLength(); i++)
        {
            Node currentNode = elementsByTagName.item(i);

            Element currentElement = (Element) currentNode;

            switch (currentAttribute)
            {
                case "city":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("id");
                    arrayListOfSubAttributes.add("name");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "coord":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("lon");
                    arrayListOfSubAttributes.add("lat");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "country":
                {
                    map.put(currentElement.getNodeName(), currentElement.getFirstChild().getNodeValue());
                    break;
                }
                case "sun":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("rise");
                    arrayListOfSubAttributes.add("set");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "temperature":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    arrayListOfSubAttributes.add("min");
                    arrayListOfSubAttributes.add("max");
                    arrayListOfSubAttributes.add("unit");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "humidity":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    arrayListOfSubAttributes.add("unit");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "pressure":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    arrayListOfSubAttributes.add("unit");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "speed":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    arrayListOfSubAttributes.add("name");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "gusts":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "direction":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    arrayListOfSubAttributes.add("code");
                    arrayListOfSubAttributes.add("name");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "clouds":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    arrayListOfSubAttributes.add("name");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "visibility":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "precipitation":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("mode");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;

                }
                case "weather":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("number");
                    arrayListOfSubAttributes.add("value");
                    arrayListOfSubAttributes.add("icon");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
                case "lastupdate":
                {
                    arrayListOfSubAttributes.clear();
                    arrayListOfSubAttributes.add("value");
                    setAmountAttributes(currentAttribute, arrayListOfSubAttributes, currentElement);
                    break;
                }
            }
        }
    }

    /**
     * adding each element attribute into a map
     *
     * @param currentAttribute - current attribute of chosen element
     * @param subAttribute - sub attribute
     * @param currentElement - all sub attributes of current element
     */
    private void setAmountAttributes(String currentAttribute, ArrayList<String> subAttribute, Element currentElement)
    {
        for (String aSubAttribute : subAttribute)
        {
            map.put(currentAttribute + aSubAttribute, currentElement.getAttribute(aSubAttribute));
        }
    }

    /**
     * initializes class WeatherData with the maps data
     *
     * @return reference to WeatherData object
     */
    private WeatherData sendToWeatherData() throws WeatherDataServiceException
    {
        weatherData = new WeatherData(map);

        return weatherData;
    }
}
package com.golodantonsamiradanielgreidyshani.OpenWeatherMap;

/**
 * class Location values received by user
 */
public class Location
{
    /**
     * variable temperatureUnit is initialized to the unit "metric" by default
     */
    private String city = null;
    private String country = null;
    private static String temperatureUnit = "metric";

    /**
     * the constructor receives the params written below
     * @param city - city name typed by user
     * @param country - country id typed by user
     * @throws WeatherDataServiceException
     */
    public Location(String city, String country) throws WeatherDataServiceException
    {
        setCity(city);
        setCountry(country);
    }

    /**
     * @return city name of type String
     */
    public String getCity()
    {
        return city;
    }

    /**
     * sets city name received by user
     * checks whether input has numbers or at least one letter
     * reduces spaces between words, checking for correct input
     * @param city - city name of type string
     * @throws WeatherDataServiceException
     */
    private void setCity(String city) throws WeatherDataServiceException
    {
        if(!checkIfNumber(city))
        {
            this.city = city.trim().replaceAll(" ", "-");
        }
    }

    /**
     * @return country code of type String
     */
    public String getCountry()
    {
        return country;
    }

    /**
     * sets country code received by user
     * checks that the input has no numbers
     * the variable does not have to be initialized
     * @param country - country id of type string
     * @throws WeatherDataServiceException
     */
    private void setCountry(String country) throws WeatherDataServiceException
    {
        if(!checkIfNumber(country))
        {
            this.country = country;
        }
    }

    /**
     * @return temperaturUnit of type String
     */
    public static String getTemperatureUnit()
    {
        return temperatureUnit;
    }

    /**
     * sets temperature unit received by user
     * @param temperatureUnit - temperature unit of type string
     */
    public static void setTemperatureUnit(String temperatureUnit)
    {
        Location.temperatureUnit = temperatureUnit;
    }

    /**
     * checks whether the input from user contains at least one number
     * @param strFromUser - gets the string input from the user
     * @return false when input is not a number
     * @throws WeatherDataServiceException
     */
    private boolean checkIfNumber(String strFromUser) throws WeatherDataServiceException
    {
        boolean isNumber = false;

        try
        {
            Integer.parseInt(strFromUser);
            throw new WeatherDataServiceException("Please enter only letters");
        }
        catch (NumberFormatException e)
        {
        }

        return isNumber;
    }
}
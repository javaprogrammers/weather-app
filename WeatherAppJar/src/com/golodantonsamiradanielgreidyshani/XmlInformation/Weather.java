package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - number, value and icon
 * attributes received from maps data
 */
public class Weather
{
    private double number;
    private String value;
    private String icon;

    /**
     * the constructor receives the params written below
     *
     * @param number - weather condition id
     * @param value - weather condition name
     * @param icon - weather icon id
     */
    public Weather(String number, String value, String icon)
    {
        try
        {
            setNumber(number);
            setValue(value);
            setIcon(icon);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return weather condition id (of type double)
     */
    public double getNumber()
    {
        return number;
    }

    /**
     * sets value, received from maps data
     * parses from string (gotten from map) to double
     * checks if the id value is greater or less than 0
     *
     * @param number - weather condition id
     */
    private void setNumber(String number)
    {
        if(number.length() != 0)
        {
            try
            {
                double valueNumber = Double.parseDouble(number);

                if(valueNumber >= 0)
                {
                    this.number = valueNumber;
                }
                else
                {
                    throw new NumberFormatException("Incorrect weather condition ID");
                }
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Weather parsing problem");
            }
        }
    }

    /**
     * @return value of weather condition name (of type string)
     */
    public String getValue()
    {
        return value;
    }

    /**
     * sets weather condition name received from maps data
     *
     * @param value - of weather condition name
     */
    private void setValue(String value)
    {
        this.value = value;
    }

    /**
     * @return weather icon (of type string)
     */
    public String getIcon()
    {
        return icon;
    }

    /**
     * sets weather icon received from maps data
     *
     * @param icon - weather icon
     */
    private void setIcon(String icon)
    {
        this.icon = icon;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return "Weather{" +
               "number=" + number +
               ", value='" + value + '\'' +
               ", icon='" + icon + '\'' +
               '}';
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Weather)) return false;

        Weather weather = (Weather) o;

        return Double.compare(weather.getNumber(), getNumber()) == 0 &&
               !(getValue() != null ? !getValue().equals(weather.getValue()) : weather.getValue() != null) &&
               !(getIcon() != null ? !getIcon().equals(weather.getIcon()) : weather.getIcon() != null);

    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getNumber());
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getValue() != null ? getValue().hashCode() : 0);
        result = 31 * result + (getIcon() != null ? getIcon().hashCode() : 0);
        return result;
    }
}

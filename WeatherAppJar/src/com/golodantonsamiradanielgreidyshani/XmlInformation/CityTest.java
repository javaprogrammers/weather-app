package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Nathan on 24/08/2015.
 */
public class CityTest
{
    private int ID;
    private String name;
    private String countryCode;

    private Coord coord;
    private Sun sun;

    @Before
    public void setUp() throws Exception
    {
        ID = 294751;
        name = "holon";
        countryCode = "IL";
        coord = new Coord("30", "40");
        sun = new Sun("2015-05-15T05:20:21", "2015-05-15T18:27:33");
    }

    @After
    public void tearDown() throws Exception
    {
        name = null;
        countryCode = null;
        coord = null;
        sun = null;
    }

    @Test
    public void testGetID() throws Exception
    {
        int expected = 294751;
        int actual = ID;

        assertEquals(expected, actual);
    }

    @Test
    public void testGetName() throws Exception
    {
        String expected = "holon";
        String actual = name;

        assertEquals(expected, actual);
    }

    @Test
    public void testGetCountryCode() throws Exception
    {
        String expected = "IL";
        String actual = countryCode;

        assertEquals(expected, actual);
    }

    @Test
    public void testGetCoord() throws Exception
    {
        Coord expected = coord;

        assertNotNull(expected);
    }

    @Test
    public void testGetSun() throws Exception
    {
        Sun expected = sun;

        assertNotNull(expected);
    }
}
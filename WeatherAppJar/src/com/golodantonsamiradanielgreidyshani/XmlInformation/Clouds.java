package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - value and name
 * attributes received from maps data
 */
public class Clouds
{
    private double value;
    private String name;

    /**
     * the constructor receives the params written below
     *
     * @param value - cloudiness value
     * @param name - name of the cloudiness
     */
    public Clouds(String value, String name)
    {
        try
        {
            setValue(value);
            setName(name);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return cloudiness value (of type int)
     */
    public double getValue()
    {
        return value;
    }

    /**
     * sets the value, received from maps data
     * parses from a string (gotten from map) to double
     * checks if the value is greater or less than 0
     *
     * @param value - of cloudiness
     */
    private void setValue(String value)
    {
        if(value.length() != 0)
        {
            try
            {
                double valueNumber = Double.parseDouble(value);

                if(valueNumber >= 0)
                {
                    this.value = valueNumber;
                }
                else
                {
                    throw new NumberFormatException("value of cloudiness is not correct");
                }
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Parse problem occurs");
            }
        }
    }

    /**
     * @return name of the cloudiness (of type String)
     */
    public String getName()
    {
        return name;
    }

    /**
     * sets name, received from maps data
     *
     * @param name - of the cloudiness
     */
    private void setName(String name)
    {
        this.name = name;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return name;
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Clouds)) return false;

        Clouds clouds = (Clouds) o;

        return Double.compare(clouds.getValue(), getValue()) == 0 &&
               !(getName() != null ? !getName().equals(clouds.getName()) : clouds.getName() != null);
    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getValue());
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }
}

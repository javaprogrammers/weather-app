package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CoordTest
{
    private double lon;
    private double lat;


    @Before
    public void setUp() throws Exception
    {
       lon = 34.77;
       lat = 32.01;
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGetLon() throws Exception
    {
        double expected = 34.77;
        double actual = lon;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetLat() throws Exception
    {
        double expected = 32.01;
        double actual = lat;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }
}
package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - speed, direction and gusts
 * attributes received from maps data
 */
public class Wind
{
    private Speed speed;
    private Direction direction;
    private Gusts gusts;

    /**
     * @return wind speed (of type Speed)
     */
    public Speed getSpeed()
    {
        return speed;
    }

    /**
     * sets wind speed received from maps data
     *
     * @param speed - wind speed (of type speed)
     */
    protected void setSpeed(Speed speed)
    {
        this.speed = speed;
    }

    /**
     * @return wind direction (of type Direction)
     */
    public Direction getDirection()
    {
        return direction;
    }

    /**
     * sets wind direction received from maps data
     *
     * @param direction - wind direction (of type Direction)
     */
    protected void setDirection(Direction direction)
    {
        this.direction = direction;
    }

    /**
     * @return wind gusts (of type Gusts)
     */
    public Gusts getGusts()
    {
        return gusts;
    }

    /**
     * sets wind gusts received from maps data
     *
     * @param gusts - wind gusts (of type Gusts)
     */
    protected void setGusts(Gusts gusts)
    {
        this.gusts = gusts;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return "Speed: " + getSpeed() + getDirection();
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Wind)) return false;

        Wind wind = (Wind) o;

        return !(getSpeed() != null ? !getSpeed().equals(wind.getSpeed()) : wind.getSpeed() != null) &&
               !(getDirection() != null ? !getDirection().equals(wind.getDirection()) : wind.getDirection() != null) &&
               !(getGusts() != null ? !getGusts().equals(wind.getGusts()) : wind.getGusts() != null);
    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result = getSpeed() != null ? getSpeed().hashCode() : 0;
        result = 31 * result + (getDirection() != null ? getDirection().hashCode() : 0);
        result = 31 * result + (getGusts() != null ? getGusts().hashCode() : 0);
        return result;
    }
}

package com.golodantonsamiradanielgreidyshani.XmlInformation;

import com.golodantonsamiradanielgreidyshani.OpenWeatherMap.WeatherDataServiceException;

import java.util.Map;

/**
 * this class contains the following attributes -
 * city, clouds, humidity, lastUpdate, precipitation,
 * pressure, temperature, visibility, weather, wind
 * attributes received from maps data
 */
public class WeatherData
{
    private City city;
    private Clouds clouds;
    private Humidity humidity;
    private LastUpdate lastUpdate;
    private Precipitation precipitation;
    private Pressure pressure;
    private Temperature temperature;
    private Visibility visibility;
    private Weather weather;
    private Wind wind;

    /**
     * the constructor receives a map (a Collection) 
     * 
     * @param map - contains values of each attribute
     * @throws WeatherDataServiceException
     */
    public WeatherData(Map map) throws WeatherDataServiceException
    {
        setAllAttributes(map);
    }

    /**
     * this method sets all the attributes needed in order to get full weather details
     * 
     * @param map - contains values of each attribute
     * @throws WeatherDataServiceException
     */
    private void setAllAttributes(Map map) throws WeatherDataServiceException
    {
        setCurrentAttribute("city", map);
        setCurrentAttribute("wind", map);
        setCurrentAttribute("coord", map);
        setCurrentAttribute("country", map);
        setCurrentAttribute("sun", map);
        setCurrentAttribute("temperature", map);
        setCurrentAttribute("humidity", map);
        setCurrentAttribute("pressure", map);
        setCurrentAttribute("speed", map);
        setCurrentAttribute("direction", map);
        setCurrentAttribute("clouds", map);
        setCurrentAttribute("visibility", map);
        setCurrentAttribute("precipitation", map);
        setCurrentAttribute("weather", map);
        setCurrentAttribute("lastupdate", map);
    }

    /**
     * this method sets a new instance of the current chosen attribute
     * 
     * @param currentAttribute - the chosen attribute (of type string), inserted to the switch case
     * @param map - contains values of the current attribute
     * @throws WeatherDataServiceException
     */
    private void setCurrentAttribute(String currentAttribute, Map map) throws WeatherDataServiceException
    {
        switch (currentAttribute)
        {
            case "city":
            {
                city =
                        new City(getCurrentAttribute(currentAttribute, "id", map), getCurrentAttribute(currentAttribute, "name", map));

                break;
            }
            case "wind":
            {
                wind = new Wind();

                break;
            }
            case "coord":
            {
                Coord coord =
                        new Coord(getCurrentAttribute(currentAttribute, "lon", map), getCurrentAttribute(currentAttribute, "lat", map));

                city.setCoord(coord);
                break;
            }
            case "country":
            {
                city.setCountryCode((String) map.get(currentAttribute));

                break;
            }
            case "sun":
            {
                Sun sun =
                        new Sun(getCurrentAttribute(currentAttribute, "rise", map), getCurrentAttribute(currentAttribute, "set", map));

                city.setSun(sun);
                break;
            }
            case "temperature":
            {
                temperature =
                        new Temperature(getCurrentAttribute(currentAttribute, "value", map), getCurrentAttribute(currentAttribute, "min", map), getCurrentAttribute(currentAttribute, "max", map), getCurrentAttribute(currentAttribute, "unit", map));

                break;
            }
            case "humidity":
            {
                humidity = new Humidity(getCurrentAttribute(currentAttribute, "value", map));

                break;
            }
            case "pressure":
            {
                pressure =
                        new Pressure(getCurrentAttribute(currentAttribute, "value", map), getCurrentAttribute(currentAttribute, "unit", map));

                break;
            }
            case "speed":
            {
                Speed speed =
                        new Speed(getCurrentAttribute(currentAttribute, "value", map), getCurrentAttribute(currentAttribute, "name", map));

                wind.setSpeed(speed);
                break;
            }
            case "gusts":
            {
                Gusts gusts = new Gusts(getCurrentAttribute(currentAttribute, "value", map));

                wind.setGusts(gusts);
                break;
            }
            case "direction":
            {
                Direction direction =
                        new Direction(getCurrentAttribute(currentAttribute, "value", map), getCurrentAttribute(currentAttribute, "code", map), getCurrentAttribute(currentAttribute, "name", map));

                wind.setDirection(direction);
                break;
            }
            case "clouds":
            {
                clouds =
                        new Clouds(getCurrentAttribute(currentAttribute, "value", map), getCurrentAttribute(currentAttribute, "name", map));

                break;
            }
            case "visibility":
            {
                visibility = new Visibility(getCurrentAttribute(currentAttribute, "value", map));

                break;
            }
            case "precipitation":
            {
                precipitation = new Precipitation(getCurrentAttribute(currentAttribute, "mode", map));

                break;
            }
            case "weather":
            {
                weather =
                        new Weather(getCurrentAttribute(currentAttribute, "number", map), getCurrentAttribute(currentAttribute, "value", map), getCurrentAttribute(currentAttribute, "icon", map));

                break;
            }
            case "lastupdate":
            {
                lastUpdate = new LastUpdate(getCurrentAttribute(currentAttribute, "value", map));

                break;
            }
        }
    }

    /**
     * 
     * @param firstAttribute - from map, e.g: city, temperature
     * @param subAttribute - sub attribute of firstAttribute, e.g: id, value
     * @param map - contains values of the attributes
     * @return map that contains attributes
     */
    private String getCurrentAttribute(String firstAttribute, String subAttribute, Map map)
    {
        return ((String) (map.get(firstAttribute + subAttribute)));
    }

    /**
     * @return chosen city details (of type City)
     */
    public City getCity()
    {
        return city;
    }

    /**
     * @return clouds details (of type Clouds)
     */
    public Clouds getClouds()
    {
        return clouds;
    }

    /**
     * @return humidity details (of type Humidity)
     */
    public Humidity getHumidity()
    {
        return humidity;
    }

    /**
     * @return lastUpdate details (of type LastUpdate)
     */
    public LastUpdate getLastUpdate()
    {
        return lastUpdate;
    }

    /**
     * @return precipitation details (of type Precipitation)
     */
    public Precipitation getPrecipitation()
    {
        return precipitation;
    }

    /**
     * @return pressure details (of type Pressure)
     */
    public Pressure getPressure()
    {
        return pressure;
    }

    /**
     * @return temperature details (of type Temperature)
     */
    public Temperature getTemperature()
    {
        return temperature;
    }

    /**
     * @return visibility details (of type Visibility)
     */
    public Visibility getVisibility()
    {
        return visibility;
    }

    /**
     * @return weather details (of type Weather)
     */
    public Weather getWeather()
    {
        return weather;
    }
    
    /**
     * @return wind details (of type Wind)
     */
    public Wind getWind()
    {
        return wind;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return "WeatherData{" +
               "city=" + city +
               ", clouds=" + clouds +
               ", humidity=" + humidity +
               ", lastUpdate=" + lastUpdate +
               ", precipitation=" + precipitation +
               ", pressure=" + pressure +
               ", temperature=" + temperature +
               ", visibility=" + visibility +
               ", weather=" + weather +
               ", wind=" + wind +
               '}';
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof WeatherData)) return false;

        WeatherData that = (WeatherData) o;

        return !(getCity() != null ? !getCity().equals(that.getCity()) : that.getCity() != null) &&
               !(getClouds() != null ? !getClouds().equals(that.getClouds()) : that.getClouds() != null) &&
               !(getHumidity() != null ? !getHumidity().equals(that.getHumidity()) : that.getHumidity() != null) &&
               !(getLastUpdate() != null ? !getLastUpdate().equals(that.getLastUpdate()) : that.getLastUpdate() != null) &&
               !(getPrecipitation() != null ? !getPrecipitation().equals(that.getPrecipitation()) :
                       that.getPrecipitation() != null) &&
               !(getPressure() != null ? !getPressure().equals(that.getPressure()) : that.getPressure() != null) &&
               !(getTemperature() != null ? !getTemperature().equals(that.getTemperature()) :
                       that.getTemperature() != null) &&
               !(getVisibility() != null ? !getVisibility().equals(that.getVisibility()) :
                       that.getVisibility() != null) &&
               !(getWeather() != null ? !getWeather().equals(that.getWeather()) : that.getWeather() != null) &&
               !(getWind() != null ? !getWind().equals(that.getWind()) : that.getWind() != null);

    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result = getCity() != null ? getCity().hashCode() : 0;
        result = 31 * result + (getClouds() != null ? getClouds().hashCode() : 0);
        result = 31 * result + (getHumidity() != null ? getHumidity().hashCode() : 0);
        result = 31 * result + (getLastUpdate() != null ? getLastUpdate().hashCode() : 0);
        result = 31 * result + (getPrecipitation() != null ? getPrecipitation().hashCode() : 0);
        result = 31 * result + (getPressure() != null ? getPressure().hashCode() : 0);
        result = 31 * result + (getTemperature() != null ? getTemperature().hashCode() : 0);
        result = 31 * result + (getVisibility() != null ? getVisibility().hashCode() : 0);
        result = 31 * result + (getWeather() != null ? getWeather().hashCode() : 0);
        result = 31 * result + (getWind() != null ? getWind().hashCode() : 0);
        return result;
    }
}
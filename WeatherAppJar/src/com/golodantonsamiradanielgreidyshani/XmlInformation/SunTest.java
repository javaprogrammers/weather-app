package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertNotNull;

public class SunTest
{
    private Date rise;
    private Date set;

    @Before
    public void setUp() throws Exception
    {
        rise = new Date();
        set = new Date();
    }

    @After
    public void tearDown() throws Exception
    {
        rise = null;
        set = null;
    }

    @Test
    public void testGetRise() throws Exception
    {
        Date expected = rise;

        assertNotNull(expected);
    }

    @Test
    public void testGetSet() throws Exception
    {
        Date expected = set;

        assertNotNull(expected);
    }
}
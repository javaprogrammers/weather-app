package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - value and name
 * attributes received from maps data
 */
public class Speed
{
    private double value;
    private String name;

    /**
     * the constructor receives the params written below
     *
     * @param value - wind speed, mps
     * @param name  - type of the wind
     */
    public Speed(String value, String name)
    {
        try
        {
            setValue(value);
            setName(name);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return speed value (of type double)
     */
    public double getValue()
    {
        return value;
    }

    /**
     * sets value, received from maps data
     * parses from string (gotten from map) to double
     * checks if the value is greater or less than 0
     *
     * @param value - speed value
     */
    private void setValue(String value)
    {
        if(value.length() != 0)
        {
            try
            {
                double valueNumber = Double.parseDouble(value);

                if(valueNumber >= 0)
                {
                    this.value = valueNumber;
                }
                else
                {
                    throw new NumberFormatException("value of wind speed is not correct");
                }
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Parse problem occurs");
            }
        }
    }

    /**
     * @return name - wind type (of type string)
     */
    public String getName()
    {
        return name;
    }

    /**
     * sets wind type name received from maps data
     *
     * @param name - of wind type
     */
    private void setName(String name)
    {
        this.name = name;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return name + " " + value + " m/s";
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Speed)) return false;

        Speed speed = (Speed) o;

        return Double.compare(speed.getValue(), getValue()) == 0 &&
               !(getName() != null ? !getName().equals(speed.getName()) : speed.getName() != null);

    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getValue());
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }
}

package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GustsTest
{
    private String value;

    @Before
    public void setUp() throws Exception
    {
        value = "Gentle";
    }

    @After
    public void tearDown() throws Exception
    {
        value = null;
    }

    @Test
    public void testGetValue() throws Exception
    {
        String expected = "Gentle";
        String actual = value;

        assertEquals(expected, actual);
    }
}
package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the attribute value
 * attributes received from maps data
 */
public class Visibility
{
    private double value;

    /**
     * the constructor receives the param written below
     *
     * @param value - visibility value
     */
    public Visibility(String value)
    {
        try
        {
            setValue(value);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return visibility value (of type double)
     */
    public double getValue()
    {
        return value;
    }

    /**
     * sets value, received from maps data
     * parses from string (gotten from map) to double
     * checks if the value is greater or less than 0
     *
     * @param value - visibility value
     */
    private void setValue(String value)
    {
        if(value.length() != 0)
        {
            try
            {
                this.value = Double.parseDouble(value);
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Parse problem occurs");
            }
        }
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return "Visibility{" +
               "value='" + value + '\'' +
               '}';
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Visibility)) return false;

        Visibility that = (Visibility) o;

        return Double.compare(that.getValue(), getValue()) == 0;

    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        long temp = Double.doubleToLongBits(getValue());
        return (int) (temp ^ (temp >>> 32));
    }
}

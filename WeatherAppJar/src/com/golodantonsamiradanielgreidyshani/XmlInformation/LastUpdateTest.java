package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertNotNull;

public class LastUpdateTest
{

    private Date value;

    @Before
    public void setUp() throws Exception
    {
        value = new Date();
    }

    @After
    public void tearDown() throws Exception
    {
        value = null;
    }

    @Test
    public void testGetValue() throws Exception
    {
        Date expected = value;

        assertNotNull(expected);
    }
}
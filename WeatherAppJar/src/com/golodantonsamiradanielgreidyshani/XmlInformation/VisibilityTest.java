package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VisibilityTest
{
    private double value;

    @Before
    public void setUp() throws Exception
    {
        value = 1000;
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGetValue() throws Exception
    {
        double expected = 1000;
        double actual = value;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }
}
package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Nathan on 24/08/2015.
 */
public class CloudsTest
{
    private double value;
    private String name;

    @Before
    public void setUp() throws Exception
    {
        value = 40;
        name = "scattered clouds";
    }

    @After
    public void tearDown() throws Exception
    {
        name = null;
    }

    @Test
    public void testGetValue() throws Exception
    {
        double expected = 40;
        double actual = value;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetName() throws Exception
    {
        String expected = "scattered clouds";
        String actual = name;

        assertEquals(expected, actual);
    }
}
package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class WindTest
{

    private Speed speed;
    private Direction direction;
    private Gusts gusts;

    @Before
    public void setUp() throws Exception
    {
        speed = new Speed ("4.1","Gentle breeze");
        direction = new Direction("260","W","West");
        gusts = new Gusts("Gentle");
    }

    @After
    public void tearDown() throws Exception
    {
        speed = null;
        direction = null;
        gusts = null;
    }

    @Test
    public void testGetSpeed() throws Exception
    {
        Speed expected = speed;

        assertNotNull(expected);
    }

    @Test
    public void testGetDirection() throws Exception
    {
        Direction expected = direction;

        assertNotNull(expected);
    }

    @Test
    public void testGetGusts() throws Exception
    {
        Gusts expected = gusts;

        assertNotNull(expected);
    }
}
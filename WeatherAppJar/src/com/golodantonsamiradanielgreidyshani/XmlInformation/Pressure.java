package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - value and unit
 * attributes received from maps data
 */
public class Pressure
{
    private double value;
    private String unit;

    /**
     * the constructor receives the params written below
     *
     * @param value - pressure value
      @param unit - pressure unit
     */
    public Pressure(String value, String unit)
    {
        try
        {
            setValue(value);
            setUnit(unit);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return pressure value (of type double)
     */
    public double getValue()
    {
        return value;
    }

    /**
     * sets value, received from maps data
     * parses from string (gotten from map) to double
     * checks if the value is greater or less than 0
     *
     * @param value - precipitation value
     */
    private void setValue(String value)
    {
        if(value.length() != 0)
        {
            try
            {
                double valueNumber = Double.parseDouble(value);

                if(valueNumber >= 0)
                {
                    this.value = valueNumber;
                }
                else
                {
                    throw new NumberFormatException("pressure value not correct");
                }
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Parse problem occurs");
            }
        }
    }

    /**
     * @return pressure unit (of type string)
     */
    public String getUnit()
    {
        return unit;
    }

    /**
     * sets pressure unit received from maps data
     *
     * @param unit - pressure unit
     */
    private void setUnit(String unit)
    {
        this.unit = unit;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return value + " " + unit;
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Pressure)) return false;

        Pressure pressure = (Pressure) o;

        return Double.compare(pressure.getValue(), getValue()) == 0 &&
               !(getUnit() != null ? !getUnit().equals(pressure.getUnit()) : pressure.getUnit() != null);
    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getValue());
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getUnit() != null ? getUnit().hashCode() : 0);
        return result;
    }
}

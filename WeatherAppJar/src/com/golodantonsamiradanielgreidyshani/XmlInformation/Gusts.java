package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the attribute value
 * attributes received from maps data
 */
public class Gusts {
	private String value;

	/**
     * the constructor receives the param written below
     *
     * @param value - gusts value
     */
	public Gusts(String value) {
		setValue(value);
	}

	/**
     * @return gusts value (of type string)
     */
	public String getValue() {
		return value;
	}

	/**
     * sets value received from maps data
     *
     * @param value - of the gusts
     */
	private void setValue(String value) {
		this.value = value;
	}

	
	/**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Gusts))
			return false;

		Gusts gusts = (Gusts) o;

		return !(getValue() != null ? !getValue().equals(gusts.getValue()) : gusts.getValue() != null);

	}

	/**
	 *  An override of equals for distinction between objects.
	 */
	@Override
	public int hashCode() {
		return getValue() != null ? getValue().hashCode() : 0;
	}
}

package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WeatherTest
{
    private double number;
    private String value;
    private String icon;

    @Before
    public void setUp() throws Exception
    {
        number = 802;
        value = "scattered clouds";
        icon = "03d";
    }

    @After
    public void tearDown() throws Exception
    {
        value = null;
        icon = null;
    }

    @Test
    public void testGetNumber() throws Exception
    {
        double expected = 802;
        double actual = number;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetValue() throws Exception
    {
        String expected = "scattered clouds";
        String actual = value;

        assertEquals(expected, actual);
    }

    @Test
    public void testGetIcon() throws Exception
    {
        String expected = "03d";
        String actual = icon;

        assertEquals(expected, actual);
    }
}
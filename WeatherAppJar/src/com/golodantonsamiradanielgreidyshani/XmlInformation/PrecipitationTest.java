package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrecipitationTest
{

    private String mode;

    @Before
    public void setUp() throws Exception
    {
        mode = "no";
    }

    @After
    public void tearDown() throws Exception
    {
        mode = null;
    }

    @Test
    public void testGetMode() throws Exception
    {
        String expected = "no";
        String actual = mode;

        assertEquals(expected, actual);
    }
}
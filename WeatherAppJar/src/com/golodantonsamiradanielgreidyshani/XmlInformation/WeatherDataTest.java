package com.golodantonsamiradanielgreidyshani.XmlInformation;

import com.golodantonsamiradanielgreidyshani.OpenWeatherMap.Location;
import com.golodantonsamiradanielgreidyshani.OpenWeatherMap.OpenWeatherMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class WeatherDataTest
{
    private WeatherData weatherData;
    private OpenWeatherMap openWeatherMap;

    @Before
    public void setUp() throws Exception
    {
        openWeatherMap = OpenWeatherMap.getInstance();
        weatherData = openWeatherMap.getWeatherData(new Location("holon", "IL"));
    }

    @After
    public void tearDown() throws Exception
    {
        openWeatherMap = null;
        weatherData = null;
    }

    @Test
    public void testGetCity() throws Exception
    {
        City expected = weatherData.getCity();

        assertNotNull(expected);
    }

    @Test
    public void testGetClouds() throws Exception
    {
        Clouds expected = weatherData.getClouds();

        assertNotNull(expected);
    }

    @Test
    public void testGetHumidity() throws Exception
    {
        Humidity expected = weatherData.getHumidity();

        assertNotNull(expected);
    }

    @Test
    public void testGetLastUpdate() throws Exception
    {
        LastUpdate expected = weatherData.getLastUpdate();

        assertNotNull(expected);
    }

    @Test
    public void testGetPrecipitation() throws Exception
    {
        Precipitation expected = weatherData.getPrecipitation();

        assertNotNull(expected);
    }

    @Test
    public void testGetPressure() throws Exception
    {
        Pressure expected = weatherData.getPressure();

        assertNotNull(expected);
    }

    @Test
    public void testGetTemperature() throws Exception
    {
        Temperature expected = weatherData.getTemperature();

        assertNotNull(expected);
    }

    @Test
    public void testGetVisibility() throws Exception
    {
        Visibility expected = weatherData.getVisibility();

        assertNotNull(expected);
    }

    @Test
    public void testGetWeather() throws Exception
    {
        Weather expected = weatherData.getWeather();

        assertNotNull(expected);
    }

    @Test
    public void testGetWind() throws Exception
    {
        Wind expected = weatherData.getWind();

        assertNotNull(expected);
    }
}
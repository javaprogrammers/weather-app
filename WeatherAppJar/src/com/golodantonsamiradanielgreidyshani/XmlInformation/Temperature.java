package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - value, min and max
 * attributes attributes received from maps data
 */
public class Temperature
{
    private double value;
    private double min;
    private double max;
    private TemperatureUnit unit;

    /**
     * the constructor receives the params written below
     * 
     * @param value - temperature value
     * @param min - min temperature value
     * @param max - max temperature value
     * @param temperatureUnit - kelvin, fahrenheit or celsius temperature units
     */
    public Temperature(String value, String min, String max, String temperatureUnit)
    {
        try
        {
            setValue(value);
            setMin(min);
            setMax(max);
            setUnit(temperatureUnit);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return temperature value (of type double)
     */
    public double getValue()
    {
        return value;
    }

    /**
     * sets value, received from maps data
     * parses from string (gotten from map) to double
     *
     * @param value - temperature value
     */
    private void setValue(String value)
    {
        this.value = parseToDoubleAndCheckCorrectValue(value);
    }

    /**
     * @return min value of temperature (of type double)
     */
    public double getMin()
    {
        return min;
    }

    /**
     * sets min value, received from maps data
     * parses from string (gotten from map) to double
     *
     * @param min - value of temperature
     */
    private void setMin(String min)
    {
        this.min = parseToDoubleAndCheckCorrectValue(min);
    }

    /**
     * @return max value of temperature (of type double)
     */
    public double getMax()
    {
        return max;
    }

    /**
     * sets max value, received from maps data
     * parses from string (gotten from map) to double
     *
     * @param max - value of temperature
     */
    private void setMax(String max)
    {
        this.max = parseToDoubleAndCheckCorrectValue(max);
    }

    /**
     * @return unit of temperature (of type TemperatureUnit)
     */
    public TemperatureUnit getUnit()
    {
        return unit;
    }

    /**
     * sets temperature unit, received from the cases of a switch case
     * 
     * @param unit - temperature unit
     */
    private void setUnit(String unit)
    {
        switch (unit)
        {
            case "metric":
            {
                this.unit = TemperatureUnit.metric;

                break;
            }
            case "kelvin":
            {
                this.unit = TemperatureUnit.kelvin;

                break;
            }
            case "fahrenheit":
            {
                this.unit = TemperatureUnit.imperial;

                break;
            }
            default:
            {
                this.unit = TemperatureUnit.metric;
            }
        }
    }

    /**
     * parses from string to double and checks if a correct value came from the map
     * throws an exception if a parse failed
     * throws an exception if a wrong value of coord is received
     *
     * @param value - checks for a correct temperature and parses the value
     * @return parsed number
     */
    private double parseToDoubleAndCheckCorrectValue(String value)
    {
        double valueNumber;

        if(value.length() != 0)
        {
            try
            {
                valueNumber = Double.parseDouble(value);
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Parse problem occured");
            }
        }
        else
        {
            throw new NumberFormatException("Incorrect value of temperature");
        }

        return valueNumber;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return "Temperature{" +
               "value=" + value +
               ", min=" + min +
               ", max=" + max +
               ", unit=" + unit +
               '}';
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Temperature)) return false;

        Temperature that = (Temperature) o;

        return Double.compare(that.getValue(), getValue()) == 0 && Double.compare(that.getMin(), getMin()) == 0 &&
               Double.compare(that.getMax(), getMax()) == 0 && getUnit() == that.getUnit();
    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getValue());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMin());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getMax());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getUnit() != null ? getUnit().hashCode() : 0);
        return result;
    }
}
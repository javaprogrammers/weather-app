package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - lon and lat
 * attributes received from maps data
 */
public class Coord
{
    private double lon;
    private double lat;

    /**
     * the constructor receives the params written below
     *
     * @param lon - city geo location, longitude
     * @param lat - city geo location, latitude
     */
    public Coord(String lon, String lat)
    {
        try
        {
            setLon(lon);
            setLat(lat);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return lon (of type double)
     */
    public double getLon()
    {
        return lon;
    }

    /**
     * sets lon, received from maps data
     * parses from string (gotten from map) to double
     *
     * @param lon - longitude
     */
    private void setLon(String lon)
    {
        this.lon = parseToDoubleAndCheckCorrectValue(lon);
    }

    /**
     * @return lon (of type double)
     */
    public double getLat()
    {
        return lat;
    }

    /**
     * sets lat, received from maps data
     * parses from string (gotten from map) to double
     *
     * @param lat - latitude
     */
    private void setLat(String lat)
    {
        this.lat = parseToDoubleAndCheckCorrectValue(lat);
    }

    /**
     * parses from string to double
	 * checks if a correct value came from the map
     * throws an exception if a parse failed
     * throws an exception if a wrong value of coord is received
     *
     * @param value - checks for a correct coord and parses the value
     * @return parsed number
     */
    private double parseToDoubleAndCheckCorrectValue(String value)
    {
        double valueNumber;

        if(value.length() != 0)
        {
            try
            {
                valueNumber = Double.parseDouble(value);
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Parse problem occurs");
            }
        }
        else
        {
            throw new NumberFormatException("No correct values for Coords");
        }

        return valueNumber;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return lon + "," + lat;
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Coord)) return false;

        Coord coord = (Coord) o;

        return Double.compare(coord.getLon(), getLon()) == 0 && Double.compare(coord.getLat(), getLat()) == 0;
    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getLon());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLat());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}

package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - value, code and name
 * attributes received from maps data
 */
public class Direction
{
    private double value;
    private String code;
    private String name;

    /**
     * the constructor receives the params written below
     *
     * @param value - wind direction, degrees (meteorological)
     * @param code - code of the wind direction
     * @param name - full name of the wind direction
     */
    public Direction(String value, String code, String name)
    {
        try
        {
            setValue(value);
            setCode(code);
            setName(name);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return wind direction value (of type double)
     */
    public double getValue()
    {
        return value;
    }

    /**
     * sets value, received from maps data
     * parses from string (gotten from map) to double
     * checks if the value is greater or less than 0
     *
     * @param value - wind direction
     */
    private void setValue(String value)
    {
        if(value.length() != 0)
        {
            try
            {
                double valueNumber = Double.parseDouble(value);

                if(valueNumber >= 0)
                {
                    this.value = valueNumber;
                }
                else
                {
                    throw new NumberFormatException("wind direction degrees not correct");
                }
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Parse problem occurs");
            }
        }
    }

    /**
     * @return code of the wind direction (of type string)
     */
    public String getCode()
    {
        return code;
    }

    /**
     * sets direction code received from maps data
     *
     * @param code - of the wind direction
     */
    private void setCode(String code)
    {
        this.code = code;
    }

    /**
     * @return name of wind direction (of type string)
     */
    public String getName()
    {
        return name;
    }

    /**
     * sets name received from maps data
     *
     * @param name - of the wind direction
     */
    private void setName(String name)
    {
        this.name = name;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        if(getName().length() > 0)
        {
            return " " + name + " " + value;
        }

        return "";
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Direction)) return false;

        Direction direction = (Direction) o;

        return Double.compare(direction.getValue(), getValue()) == 0 &&
               !(getCode() != null ? !getCode().equals(direction.getCode()) : direction.getCode() != null) &&
               !(getName() != null ? !getName().equals(direction.getName()) : direction.getName() != null);

    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getValue());
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getCode() != null ? getCode().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }
}
package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - id, name, countryCode, coord and sun
 * attributes received from maps data
 */

public class City {
	private int id;
	private String name;
	private String countryCode;
	private Coord coord;
	private Sun sun;

	/**
	 * the constructor receives the params written below
	 *
	 * @param id - city id
	 * @param name - city name
	 */
	public City(String id, String name) {
		try {
			setId(id);
			setName(name);
		} catch (NumberFormatException e) {
			throw new NumberFormatException(e.getMessage());
		}
	}

	/**
	 * @return id (of type int)
	 */
	public int getId() {
		return id;
	}

	/**
	 * sets id, received from maps data
	 * parses from a string (gotten from map) to int
	 * checks whether the id value is greater or equal to 0
	 * 
	 * @param id - id value
	 * @throws NumberFormatException
	 */
	private void setId(String id) throws NumberFormatException {
		if (id.length() != 0) {
			try {
				int idNumber = Integer.parseInt(id);

				if (idNumber >= 0) {
					this.id = idNumber;
				} else {
					throw new NumberFormatException("Wrong city ID");
				}
			} catch (NumberFormatException e) {
				throw new NumberFormatException("Parse problem occured");
			}
		}
	}

	/**
	 * @return city name (of type String)
	 */
	public String getName() {
		return name;
	}

	/**
	 * sets city name, received from maps data
	 *
	 * @param name - city name
	 */
	private void setName(String name) {
		this.name = name;
	}

	/**
	 * @return country code of current city (of type String)
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * sets country code, received from maps data
	 *
	 * @param countryCode - country code of current city
	 */
	protected void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return coord reference (of type Coord)
	 */
	public Coord getCoord() {
		return coord;
	}

	/**
	 * sets coord, received from maps data
	 *
	 * @param coord - reference to Coord class
	 */
	protected void setCoord(Coord coord) {
		this.coord = coord;
	}

	/**
	 * @return sun reference (of type Sun)
	 */
	public Sun getSun() {
		return sun;
	}

	/**
	 * sets sun, received from maps data
	 *
	 * @param sun - reference to Sun class
	 */
	protected void setSun(Sun sun) {
		this.sun = sun;
	}

	/**
	 * overrides toString method
	 *
	 * @return string that contains attributes data of several toString methods
	 */
	@Override
	public String toString() {
		return "City{" + "id=" + id + ", name='" + name + '\'' + ", countryCode='" + countryCode + '\'' + ", coord="
				+ coord + ", sun=" + sun + '}';
	}

	/**
	 * @param o - current object type
	 * @return boolean, true if equals, false if not
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof City))
			return false;

		City city = (City) o;

		return getId() == city.getId()
				&& !(getName() != null ? !getName().equals(city.getName()) : city.getName() != null)
				&& !(getCountryCode() != null ? !getCountryCode().equals(city.getCountryCode())
						: city.getCountryCode() != null)
				&& !(getCoord() != null ? !getCoord().equals(city.getCoord()) : city.getCoord() != null)
				&& !(getSun() != null ? !getSun().equals(city.getSun()) : city.getSun() != null);
	}

	/**
	 *  An override of equals for distinction between objects.
	 */
	@Override
	public int hashCode() {
		int result = getId();
		result = 31 * result + (getName() != null ? getName().hashCode() : 0);
		result = 31 * result + (getCountryCode() != null ? getCountryCode().hashCode() : 0);
		result = 31 * result + (getCoord() != null ? getCoord().hashCode() : 0);
		result = 31 * result + (getSun() != null ? getSun().hashCode() : 0);
		return result;
	}
}

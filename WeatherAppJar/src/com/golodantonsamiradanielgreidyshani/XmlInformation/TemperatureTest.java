package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemperatureTest
{

    private double value;
    private double min;
    private double max;
    private TemperatureUnit unit;

    @Before
    public void setUp() throws Exception
    {
        value = 31.23;
        min = 26.3;
        max = 34.6;
        unit = TemperatureUnit.metric;
    }

    @After
    public void tearDown() throws Exception
    {
        unit = null;
    }

    @Test
    public void testGetValue() throws Exception
    {
        double expected = 31.23;
        double actual = value;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetMin() throws Exception
    {
        double expected = 26.3;
        double actual = min;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetMax() throws Exception
    {
        double expected = 34.6;
        double actual = max;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetUnit() throws Exception
    {
        TemperatureUnit expected = TemperatureUnit.metric;
        TemperatureUnit actual = unit;

        assertEquals(expected, actual);
    }
}
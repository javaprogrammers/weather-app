package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DirectionTest
{
    private double value;
    private String code;
    private String name;

    @Before
    public void setUp() throws Exception
    {
        value = 260;
        code = "W";
        name = "West";
    }

    @After
    public void tearDown() throws Exception
    {
        code = null;
        name = null;
    }

    @Test
    public void testGetValue() throws Exception
    {
        double expected = 260;
        double actual = value;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetCode() throws Exception
    {
        String expected = "W";
        String actual = code;

        assertEquals(expected, actual);
    }

    @Test
    public void testGetName() throws Exception
    {
        String expected = "West";
        String actual = name;

        assertEquals(expected, actual);
    }
}
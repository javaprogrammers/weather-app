package com.golodantonsamiradanielgreidyshani.XmlInformation;

import javax.xml.ws.WebServiceException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * this class contains the following attributes - rise and set attributes
 * attributes received from maps data
 */
public class Sun {
	private Date rise;
	private Date set;

	/**
	 * the constructor receives the params written below
	 *
	 * @param rise - sunrise time (of type Date)
	 * @param set - sunset time (of type Date)
	 */
	public Sun(String rise, String set) {
		setRise(rise);
		setSet(set);
	}

	/**
	 * @return sunrise time (of type Date)
	 */
	public Date getRise() {
		return rise;
	}

	/**
	 * sets rise (sunrise) received from maps data
	 *
	 * @param rise - sunrise time
	 */
	private void setRise(String rise) {
		this.rise = getDate(rise);
	}

	/**
	 * @param rise - sunrise time
	 * @return time of sunrise (of type Date)
	 */
	private Date getDate(String rise) {
		Date date;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		long millis;

		try {
			date = simpleDateFormat.parse(rise);
			millis = date.getTime();
			date.setTime(millis + TimeZone.getDefault().getRawOffset());
		} catch (ParseException e) {
			throw new WebServiceException("Incorrect time");
		}

		return date;
	}

	/**
     * @return set - time of sunset (of type Date)
     */
	public Date getSet() {
		return set;
	}

	/**
     * sets sunset time received from maps data
     *
     * @param set - sunset time
     */
	private void setSet(String set) {
		this.set = getDate(set);
	}

	/**
	 * @return a message containing sunrise time
	 */
	public Date msgSunRise() {
		return rise;
	}

	/**
	 * @return a message containing sunset time (of type string)
	 */
	public String msgSunSet() {
		return set.toString();
	}

	/**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
	@Override
	public String toString() {
		return "Sun [rise=" + rise + ", set=" + set + "]";
	}

	/**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Sun))
			return false;

		Sun sun = (Sun) o;

		return !(getRise() != null ? !getRise().equals(sun.getRise()) : sun.getRise() != null)
				&& !(getSet() != null ? !getSet().equals(sun.getSet()) : sun.getSet() != null);

	}

	/**
	 *  An override of equals for distinction between objects.
	 */
	@Override
	public int hashCode() {
		int result = getRise() != null ? getRise().hashCode() : 0;
		result = 31 * result + (getSet() != null ? getSet().hashCode() : 0);
		return result;
	}
}

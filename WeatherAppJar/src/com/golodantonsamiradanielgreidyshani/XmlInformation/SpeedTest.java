package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpeedTest
{
    private double value;
    private String name;

    @Before
    public void setUp() throws Exception
    {
        value = 4.1;
        name = "Gentle breeze";
    }

    @After
    public void tearDown() throws Exception
    {
        name = null;
    }

    @Test
    public void testGetValue() throws Exception
    {
        double expected = 4.1;
        double actual = value;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetName() throws Exception
    {
        String expected = "Gentle breeze";
        String actual = name;

        assertEquals(expected, actual);
    }
}
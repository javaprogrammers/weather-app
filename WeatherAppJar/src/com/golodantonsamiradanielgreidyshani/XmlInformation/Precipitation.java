package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the attribute mode
 * attributes received from maps data
 */
public class Precipitation
{
    private String mode;

    /**
     * the constructor receives the param written below
     *
     * @param mode - of the precipitation
     */
    public Precipitation(String mode)
    {
        setMode(mode);
    }

    /**
     * @return precipitation mode (of type string)
     */
    public String getMode()
    {
        return mode;
    }

    /**
     * sets precipitation mode received from maps data
     *
     * @param mode - precipitation mode
     */
    private void setMode(String mode)
    {
        this.mode = mode;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return "Precipitation{" +
               "mode='" + mode + '\'' +
               '}';
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Precipitation)) return false;

        Precipitation that = (Precipitation) o;

        return !(getMode() != null ? !getMode().equals(that.getMode()) : that.getMode() != null);

    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        return getMode() != null ? getMode().hashCode() : 0;
    }
}
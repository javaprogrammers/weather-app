package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * enum that contains the three temperature units written below
 */
public enum TemperatureUnit
    {
        metric,
        imperial,
        kelvin
    }

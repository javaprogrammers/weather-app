package com.golodantonsamiradanielgreidyshani.XmlInformation;

/**
 * this class contains the following attributes - value and unit (in percent)
 * attributes received from maps data
 */
public class Humidity
{
    private double value;
    private final char unit = '%';

    /**
     * the constructor receives the params written below
     *
     * @param value - humidity value
     */
    public Humidity(String value)
    {
        try
        {
            setValue(value);
        }
        catch (NumberFormatException e)
        {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * @return humidity value (of type double)
     */
    public double getValue()
    {
        return value;
    }

    /**
     * sets value, received from maps data
     * parses from string (gotten from map) to double
     * checks if the value is greater or less than 0
     *
     * @param value - wind direction, degrees
     */
    private void setValue(String value)
    {
        if(value.length() != 0)
        {
            try
            {
                double valueNumber = Double.parseDouble(value);

                if(valueNumber >= 0)
                {
                    this.value = valueNumber;
                }
                else
                {
                    throw new NumberFormatException("humidity value is not correct");
                }
            }
            catch (NumberFormatException e)
            {
                throw new NumberFormatException("Parse problem occurs");
            }
        }
    }

    /**
     * @return humidity unit (of type char)
     */
    public char getUnit()
    {
        return unit;
    }

    /**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
    @Override
    public String toString()
    {
        return String.valueOf(value) + unit;
    }

    /**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(!(o instanceof Humidity)) return false;

        Humidity humidity = (Humidity) o;

        return Double.compare(humidity.getValue(), getValue()) == 0 && getUnit() == humidity.getUnit();

    }

    /**
     *  An override of equals for distinction between objects.
     */
    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getValue());
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) getUnit();
        return result;
    }
}

package com.golodantonsamiradanielgreidyshani.XmlInformation;

import javax.xml.ws.WebServiceException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * this class contains the following attributes - value
 * attributes received from maps data
 */
public class LastUpdate {
	private Date value;

	/**
     * the constructor receives the param written below
     *
     * @param value - last time when data was updated
     */
	public LastUpdate(String value) {
		setValue(value);
	}

	/**
     * @return date value (of type Date)
     */
	public Date getValue() {
		return value;
	}

	/**
	 * sets last update values
	 * throws an exception when the time is wrong
	 * 
	 * @param value - last update date value
	 */
	private void setValue(String value) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date lastUpdate;
		long mills;

		try {
			lastUpdate = sdf.parse(value);
			mills = lastUpdate.getTime();
			lastUpdate.setTime(mills + TimeZone.getDefault().getRawOffset());
		} catch (ParseException e) {
			throw new WebServiceException("Incorrect time");
		}

		this.value = lastUpdate;
	}

	/**
     * overrides toString method
     *
     * @return string that contains attributes data of several toString methods
     */
	@Override
	public String toString() {
		return "LastUpdate{" + "value='" + value.toString() + '\'' + '}';
	}

	/**
     * @param o - current object type
     * @return boolean, true if equals, false if not
     */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof LastUpdate))
			return false;

		LastUpdate that = (LastUpdate) o;

		return !(getValue() != null ? !getValue().equals(that.getValue()) : that.getValue() != null);

	}

	/**
	 *  An override of equals for distinction between objects.
	 */
	@Override
	public int hashCode() {
		return getValue() != null ? getValue().hashCode() : 0;
	}
}
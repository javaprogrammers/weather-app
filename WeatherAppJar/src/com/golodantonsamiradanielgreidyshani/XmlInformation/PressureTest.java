package com.golodantonsamiradanielgreidyshani.XmlInformation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PressureTest
{
    private double value;
    private String unit;

    @Before
    public void setUp() throws Exception
    {
        value = 1008;
        unit = "hPa";
    }

    @After
    public void tearDown() throws Exception
    {
        unit = null;
    }

    @Test
    public void testGetValue() throws Exception
    {
        double expected = 1008;
        double actual = value;

        assertEquals(String.valueOf(expected), String.valueOf(actual));
    }

    @Test
    public void testGetUnit() throws Exception
    {
        String expected = "hPa";
        String actual = unit;

        assertEquals(expected, actual);
    }
}